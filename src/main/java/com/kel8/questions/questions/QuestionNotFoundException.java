package com.kel8.questions.questions;

class QuestionNotFoundException extends RuntimeException {
    QuestionNotFoundException(Long id){
        super("Could not find questions " + id);
    }
}
