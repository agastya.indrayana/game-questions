package com.kel8.questions.questions;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;

@Data
@Entity
public class Question {

    private @Id @GeneratedValue Long id;
    private String question;
    private ArrayList<String> choice;
    private String answer;
    private String hint;

    Question(){}

    Question(String question, ArrayList<String> choice, String answer, String hint){
        this.question = question;
        this.choice = choice;
        this.answer = answer;
        this.hint = hint;
    }
}
