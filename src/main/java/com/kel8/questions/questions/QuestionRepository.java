package com.kel8.questions.questions;

import org.springframework.data.jpa.repository.JpaRepository;

interface QuestionRepository extends JpaRepository<Question,Long> {

}
