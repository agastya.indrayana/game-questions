package com.kel8.questions.questions;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuestionController {
    private final QuestionRepository repository;
    QuestionController(QuestionRepository repository){
        this.repository = repository;
    }

//    root
    @GetMapping("/api/questions")
    List<Question> all(){
        return repository.findAll();
    }

    @PostMapping("/api/questions")
    Question newQestions(@RequestBody Question newQuestions){
        return repository.save(newQuestions);
    }

//    singgle item

    @GetMapping("/api/questions/{id}")
    Question one(@PathVariable long id){
        return repository.findById(id).orElseThrow(()->new QuestionNotFoundException(id));
    }

    @DeleteMapping("/api/questions/{id}")
    void deleteQuestion(@PathVariable long id){
        repository.deleteById(id);
    }


}
